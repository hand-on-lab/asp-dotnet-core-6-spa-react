# asp-dotnet-core-6-spa-react

Hands on Lab for ASP.NET Core with React on SPA

## Solve .NET 6 can't build on macOS

![](images/xcrun_error_01.png)

如果遇到下面錯誤：

>Failed to sign apphost with error code 0: xcrun: error: invalid active developer path (/Library/Developer/CommandLineTools), missing xcrun at: /Library/Developer/CommandLineTools/usr/bin/xcrun
>			/Users/ct.tsai/Desktop/Repo/gitlab/asp-dotnet-core-6-spa-react/src/AspNeCore6WithReactSpa/Application.Web/obj/Debug/net6.0/apphost: the codesign_allocate helper tool cannot be found or used
>			at (536:5)

請記得透過以下指令更新 xcode 延伸套件：

```sh
xcode-select --install
```

![](images/xcrun_error_02.png)

![](images/xcrun_error_03.png)

![](images/xcrun_error_04.png)

下載後會自動安裝，安裝完成後接著回去重啟 IDE(以我這邊為例是 Rider, 即可成功編譯)